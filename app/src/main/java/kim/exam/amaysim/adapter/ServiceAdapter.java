package kim.exam.amaysim.adapter;

/**
 * Created by Posible on 10/6/2016.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import kim.exam.amaysim.R;
import kim.exam.amaysim.model.ProductInfo;
import kim.exam.amaysim.model.ServicesInfo;


public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<ServicesInfo> mData;
    private ServicesInfo item;
    public ServiceAdapter(Context context, ArrayList<ServicesInfo> mData) {
        mContext = context;
        this.mData = mData;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public ServiceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_services, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        item = mData.get(position);

        holder.tvMsn.setText("MSN: " + item.msn);
        holder.tvCredit.setText("Credit: " + item.credit);
        holder.tvCreditXpiry.setText("Credit Expiry: " + item.creditExpiry);
    }
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvMsn;
        public TextView tvCredit;
        public TextView tvCreditXpiry;
        public ViewHolder(View v) {
            super(v);
            tvMsn = (TextView) v.findViewById(R.id.text_msn);
            tvCredit = (TextView) v.findViewById(R.id.text_credit);
            tvCreditXpiry = (TextView) v.findViewById(R.id.text_credit_xpiry);
        }
    }
}
