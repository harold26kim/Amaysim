package kim.exam.amaysim.adapter;

/**
 * Created by Posible on 10/6/2016.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import kim.exam.amaysim.R;
import kim.exam.amaysim.model.ProductInfo;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<ProductInfo> mData;
    private ProductInfo item;
    public ProductAdapter(Context context, ArrayList<ProductInfo> mData) {
        mContext = context;
        this.mData = mData;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_products, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        item = mData.get(position);

        holder.tvName.setText("Name: " + item.name);
        holder.tvPrice.setText("Price: " + item.price);
        holder.tvUnliText.setText("Unlimited Text: " + item.unliText);
        holder.tvUnliCall.setText("Unlimited Call: " + item.unliTalk);
        holder.tvInternationalUnliText.setText("International Unlimited Text: " + item.unliInternationalText);
        holder.tvInternationalUnliCall.setText("International Unlimited Call: " + item.unliInternationalTalk);

    }
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvPrice;
        public TextView tvUnliText;
        public TextView tvUnliCall;
        public TextView tvInternationalUnliCall;
        public TextView tvInternationalUnliText;
        public ViewHolder(View v) {
            super(v);
            tvName = (TextView) v.findViewById(R.id.text_name);
            tvPrice = (TextView) v.findViewById(R.id.text_price);
            tvUnliText = (TextView) v.findViewById(R.id.text_unli_text);
            tvUnliCall = (TextView) v.findViewById(R.id.text_unli_call);
            tvInternationalUnliCall = (TextView) v.findViewById(R.id.text_international_unli_call);
            tvInternationalUnliText = (TextView) v.findViewById(R.id.text_international_unli_text);
        }
    }
}
