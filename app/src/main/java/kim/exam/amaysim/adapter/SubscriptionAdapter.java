package kim.exam.amaysim.adapter;

/**
 * Created by Posible on 10/6/2016.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import kim.exam.amaysim.R;
import kim.exam.amaysim.model.ProductInfo;
import kim.exam.amaysim.model.SubscriptionsInfo;


public class SubscriptionAdapter extends RecyclerView.Adapter<SubscriptionAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<SubscriptionsInfo> mData;
    private SubscriptionsInfo item;
    public SubscriptionAdapter(Context context, ArrayList<SubscriptionsInfo> mData) {
        mContext = context;
        this.mData = mData;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public SubscriptionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_subscription, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        item = mData.get(position);

        holder.tvDataBalance.setText("Included Data Balance: " + item.dataBalance);
        holder.tvExpiryDate.setText("Expiry Date: " + item.expiryDate);
        holder.tvAutoRenewal.setText("Auto Renewal: " + item.autoRenewal);
        holder.tvSubscription.setText("Primary Subscription: " + item.primarySubscription);
    }
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvDataBalance;
        public TextView tvExpiryDate;
        public TextView tvAutoRenewal;
        public TextView tvSubscription;
        public ViewHolder(View v) {
            super(v);
            tvDataBalance = (TextView) v.findViewById(R.id.text_included_data_balance);
            tvExpiryDate = (TextView) v.findViewById(R.id.text_expiry_date);
            tvAutoRenewal = (TextView) v.findViewById(R.id.text_auto_renewal);
            tvSubscription = (TextView) v.findViewById(R.id.primary_subscription);
        }
    }
}
