package kim.exam.amaysim.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import kim.exam.amaysim.fragment.pager.MyInfoFragment;
import kim.exam.amaysim.fragment.pager.ProductFragment;
import kim.exam.amaysim.fragment.pager.ServiceFragment;
import kim.exam.amaysim.fragment.pager.SubscriptionFragment;
import kim.exam.amaysim.model.ProductInfo;
import kim.exam.amaysim.model.ServicesInfo;
import kim.exam.amaysim.model.SubscriptionsInfo;
import kim.exam.amaysim.model.UserInfo;

/**
 * Created by Kim PC on 12/22/2016.
 */

public class PagerFragmentAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 4;
    private String tabTitles[] = new String[] { "My Info", "Services", "Subscription", "Product" };
    private UserInfo userInfo;
    public PagerFragmentAdapter(FragmentManager fragmentManager, UserInfo userInfo) {
        super(fragmentManager);
        this.userInfo = userInfo;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
        case 0:
            return MyInfoFragment.newInstance(userInfo);
        case 1:
            return ServiceFragment.newInstance((ArrayList<ServicesInfo>) userInfo.lstServices);
        case 2:
            return SubscriptionFragment.newInstance((ArrayList<SubscriptionsInfo>) userInfo.lstSubscriptionsInfo);
        case 3:
            return ProductFragment.newInstance((ArrayList<ProductInfo>) userInfo.lstProduct);
        }
        return new Fragment();
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
