package kim.exam.amaysim.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kim PC on 12/22/2016.
 */

public class ProductInfo implements Parcelable {
    public String type;
    public String id;
    public String name;
    public String includedData;
    public String includedCredit;
    public String includedInternationalTalk;
    public boolean unliText;
    public boolean unliTalk;
    public boolean unliInternationalText;
    public boolean unliInternationalTalk;
    public double price;

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(includedData);
        dest.writeString(includedCredit);
        dest.writeString(includedInternationalTalk);
        dest.writeByte((byte) (unliText ? 1 : 0));
        dest.writeByte((byte) (unliTalk ? 1 : 0));
        dest.writeByte((byte) (unliInternationalText ? 1 : 0));
        dest.writeByte((byte) (unliInternationalTalk ? 1 : 0));
        dest.writeDouble(price);
    }

    public ProductInfo(){

    }
    public ProductInfo(String type, String id, String name, String includedData, String includedCredit,
                       String includedInternationalTalk, boolean unliText, boolean unliTalk,
                       boolean unliInternationalText, boolean unliInternationalTalk, double price){
        this.type = type;
        this.id = id;
        this.name = name;
        this.includedData = includedData;
        this.includedCredit = includedCredit;
        this.includedInternationalTalk = includedInternationalTalk;
        this.unliText = unliText;
        this.unliTalk = unliTalk;
        this.unliInternationalText = unliInternationalText;
        this.unliInternationalTalk = unliInternationalTalk;
        this.price = price;
    }


    private ProductInfo(Parcel in){
        this.type = in.readString();
        this.id = in.readString();
        this.name = in.readString();
        this.includedData = in.readString();
        this.includedCredit = in.readString();
        this.includedInternationalTalk = in.readString();
        this.unliText = in.readByte() != 0;
        this.unliTalk = in.readByte() != 0;
        this.unliInternationalText = in.readByte() != 0;
        this.unliInternationalTalk = in.readByte() != 0;
        this.price = in.readDouble();
    }

    public static final Parcelable.Creator<ProductInfo> CREATOR = new Parcelable.Creator<ProductInfo>() {

        @Override
        public ProductInfo createFromParcel(Parcel source) {
            return new ProductInfo(source);
        }

        @Override
        public ProductInfo[] newArray(int size) {
            return new ProductInfo[size];
        }
    };
}
