package kim.exam.amaysim.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kim PC on 12/22/2016.
 */

public class SubscriptionsInfo implements Parcelable {
    public String type;
    public String id;
    public double dataBalance;
    public String creditBalance;
    public String rollOverCreditBalance;
    public String rollOverDataBalance;
    public String internationalTalkBalance;
    public String expiryDate;
    public boolean autoRenewal;
    public boolean primarySubscription;
    public String links;
    public String servicesLinks;
    public String productType;
    public String productId;
    public String downgrade;

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(id);
        dest.writeDouble(dataBalance);
        dest.writeString(creditBalance);
        dest.writeString(rollOverCreditBalance);
        dest.writeString(rollOverDataBalance);
        dest.writeString(internationalTalkBalance);
        dest.writeString(expiryDate);
        dest.writeByte((byte) (autoRenewal ? 1 : 0));
        dest.writeByte((byte) (primarySubscription ? 1 : 0));
        dest.writeString(links);
        dest.writeString(servicesLinks);
        dest.writeString(productType);
        dest.writeString(productId);
        dest.writeString(downgrade);
    }

    public SubscriptionsInfo(){}

    private SubscriptionsInfo(Parcel in){
        this.type = in.readString();
        this.id = in.readString();
        this.dataBalance = in.readDouble();
        this.creditBalance = in.readString();
        this.rollOverCreditBalance = in.readString();
        this.rollOverDataBalance = in.readString();
        this.internationalTalkBalance = in.readString();
        this.expiryDate = in.readString();
        this.autoRenewal = in.readByte() != 0;
        this.primarySubscription = in.readByte() != 0;
        this.links = in.readString();
        this.servicesLinks = in.readString();
        this.productType = in.readString();
        this.productId = in.readString();
        this.downgrade = in.readString();
    }

    public static final Parcelable.Creator<SubscriptionsInfo> CREATOR = new Parcelable.Creator<SubscriptionsInfo>() {

        @Override
        public SubscriptionsInfo createFromParcel(Parcel source) {
            return new SubscriptionsInfo(source);
        }

        @Override
        public SubscriptionsInfo[] newArray(int size) {
            return new SubscriptionsInfo[size];
        }
    };
}
