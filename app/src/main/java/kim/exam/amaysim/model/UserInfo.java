package kim.exam.amaysim.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Kim PC on 12/22/2016.
 */

public class UserInfo implements Parcelable {
    public String type;
    public String id;
    public String paymentType;
    public String unbilledCharges;
    public String nextBillingDate;
    public String title;
    public String firstName;
    public String lastName;
    public String dateOfBirth;
    public String contactNumber;
    public String email;
    public boolean emailVerified;
    public boolean emailSubscriptionStatus;
    public String link;
    public String servicesLink;
    public List<ServicesInfo> lstServices;
    public List<ProductInfo> lstProduct;
    public List<SubscriptionsInfo> lstSubscriptionsInfo;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(id);
        dest.writeString(paymentType);
        dest.writeString(unbilledCharges);
        dest.writeString(nextBillingDate);
        dest.writeString(title);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(dateOfBirth);
        dest.writeString(contactNumber);
        dest.writeString(email);
        dest.writeByte((byte) (emailVerified ? 1 : 0));
        dest.writeByte((byte) (emailSubscriptionStatus ? 1 : 0));
        dest.writeString(link);
        dest.writeString(servicesLink);

        dest.writeTypedList(lstServices);
        dest.writeTypedList(lstProduct);
        dest.writeTypedList(lstSubscriptionsInfo);
    }

    public UserInfo(){

    }
    public UserInfo(String type, String id, String paymentType, String unbilledCharges, String nextBillingDate,
                    String title, String firstName, String lastName, String dateOfBirth,
                    String contactNumber, String email, boolean emailVerified, boolean emailSubscriptionStatus,
                    String link, String servicesLink, List<ServicesInfo> lstServices, List<ProductInfo> lstProduct,
                    List<SubscriptionsInfo> lstSubscriptionsInfo){
        this.type = type;
        this.id = id;
        this.paymentType = paymentType;
        this.unbilledCharges = unbilledCharges;
        this.nextBillingDate = nextBillingDate;
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.contactNumber = contactNumber;
        this.email = email;
        this.emailVerified = emailVerified;
        this.emailSubscriptionStatus = emailSubscriptionStatus;
        this.link = link;
        this.servicesLink = servicesLink;

        this.lstServices = lstServices;
        this.lstProduct = lstProduct;
        this.lstSubscriptionsInfo = lstSubscriptionsInfo;
    }

    private UserInfo(Parcel in){
        this.type = in.readString();
        this.id = in.readString();
        this.paymentType = in.readString();
        this.unbilledCharges = in.readString();
        this.nextBillingDate = in.readString();
        this.title = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.dateOfBirth = in.readString();
        this.contactNumber = in.readString();
        this.email = in.readString();
        this.emailVerified = in.readByte() != 0;
        this.emailSubscriptionStatus = in.readByte() != 0;
        this.link = in.readString();
        this.servicesLink = in.readString();

        in.readTypedList(lstServices, ServicesInfo.CREATOR);
        in.readTypedList(lstProduct, ProductInfo.CREATOR);
        in.readTypedList(lstSubscriptionsInfo, SubscriptionsInfo.CREATOR);

    }

    public static final Parcelable.Creator<UserInfo> CREATOR = new Parcelable.Creator<UserInfo>() {

        @Override
        public UserInfo createFromParcel(Parcel source) {
            return new UserInfo(source);
        }

        @Override
        public UserInfo[] newArray(int size) {
            return new UserInfo[size];
        }
    };
}
