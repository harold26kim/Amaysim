package kim.exam.amaysim.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kim PC on 12/22/2016.
 */



public class ServicesDataInfo implements Parcelable {
    public String type;
    public String id;

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(id);
    }

    public ServicesDataInfo(){

    }
    public ServicesDataInfo(String type, String id){
        this.type = type;
        this.id = id;
    }

    private ServicesDataInfo(Parcel in){
        this.type = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<ServicesDataInfo> CREATOR = new Parcelable.Creator<ServicesDataInfo>() {

        @Override
        public ServicesDataInfo createFromParcel(Parcel source) {
            return new ServicesDataInfo(source);
        }

        @Override
        public ServicesDataInfo[] newArray(int size) {
            return new ServicesDataInfo[size];
        }
    };
}
