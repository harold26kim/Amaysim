package kim.exam.amaysim.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Kim PC on 12/22/2016.
 */

public class ServicesInfo implements Parcelable {
    public String type;
    public String id;
    public String msn;
    public String credit;
    public String creditExpiry;
    public boolean dataUsage;
    public String link;
    public String subscriptionLinks;
    public List<ServicesDataInfo> lstServicesDataInfo;

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(id);
        dest.writeString(msn);
        dest.writeString(credit);
        dest.writeString(creditExpiry);
        dest.writeByte((byte) (dataUsage ? 1 : 0));
        dest.writeString(link);
        dest.writeString(subscriptionLinks);
        dest.writeTypedList(lstServicesDataInfo);
    }

    public ServicesInfo(){

    }
    public ServicesInfo(String type, String id, String msn, String credit, String creditExpiry, boolean dataUsage,
                        String link, String subscriptionLinks, List<ServicesDataInfo> lstServicesDataInfo){
        this.type = type;
        this.id = id;
        this.msn = msn;
        this.credit = credit;
        this.creditExpiry = creditExpiry;
        this.dataUsage = dataUsage;
        this.link = link;
        this.subscriptionLinks = subscriptionLinks;
        this.lstServicesDataInfo = lstServicesDataInfo;
    }


    private ServicesInfo(Parcel in){
        this.type = in.readString();
        this.id = in.readString();
        this.msn = in.readString();
        this.credit = in.readString();
        this.creditExpiry = in.readString();
        this.dataUsage = in.readByte() != 0;
        this.link = in.readString();
        this.subscriptionLinks = in.readString();

        in.readTypedList(lstServicesDataInfo, ServicesDataInfo.CREATOR);
    }

    public static final Parcelable.Creator<ServicesInfo> CREATOR = new Parcelable.Creator<ServicesInfo>() {

        @Override
        public ServicesInfo createFromParcel(Parcel source) {
            return new ServicesInfo(source);
        }

        @Override
        public ServicesInfo[] newArray(int size) {
            return new ServicesInfo[size];
        }
    };
}
