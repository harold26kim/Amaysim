package kim.exam.amaysim.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;

import kim.exam.amaysim.MainActivity;
import kim.exam.amaysim.R;

/**
 * Created by Kim PC on 12/22/2016.
 */

public class SplashScreenFragment extends Fragment {

    private MainActivity main;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_splash_screen, container, false);

        main = (MainActivity) getActivity();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        main = (MainActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( doSleep == null ) {
            doSleep = new DoSleep();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                doSleep.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                doSleep.execute();
            }
        }
    }

    private DoSleep doSleep;
    private class DoSleep extends AsyncTask<Void, Long, Boolean> {
        private String jsonCollections;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Thread.sleep(3000);
                jsonCollections = loadJSONFromAsset();
                main.jsonCollections = jsonCollections;
                return true;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        public String loadJSONFromAsset() throws IOException {
            String json = null;
            InputStream is = getActivity().getAssets().open("collection.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            return json;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            doSleep = null;
            if ( result ) {
                main.switchFragment(new LoginFragment());
            } else {
                Toast.makeText(main, getString(R.string.msg_failed_to_load_json), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            doSleep = null;
        }
    }
}
