package kim.exam.amaysim.fragment.pager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kim.exam.amaysim.MainActivity;
import kim.exam.amaysim.R;
import kim.exam.amaysim.model.UserInfo;
import kim.exam.amaysim.util.Constant;

/**
 * Created by Kim PC on 12/22/2016.
 */

public class MyInfoFragment extends Fragment {

    private MainActivity main;
    private UserInfo userInfo;

    public static Fragment newInstance(UserInfo userInfo) {
        MyInfoFragment fragment = new MyInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.BUNDLE_USER_INFO, userInfo);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_pager_my_info, container, false);

        main = (MainActivity) getActivity();
        userInfo = getArguments().getParcelable(Constant.BUNDLE_USER_INFO);

        setUpViews(view);
        return view;
    }

    private void setUpViews(View v) {
        ((TextView) v.findViewById(R.id.text_name)).setText("Name: " + userInfo.title + ". " +
                userInfo.firstName + " " + userInfo.lastName
        );

        ((TextView) v.findViewById(R.id.text_contact_no)).setText("Contact No.: " + userInfo.contactNumber);

        ((TextView) v.findViewById(R.id.text_email)).setText("Email Address: " + userInfo.email);

        ((TextView) v.findViewById(R.id.text_bday)).setText("Date of Birth: " + userInfo.dateOfBirth);
    }
}
