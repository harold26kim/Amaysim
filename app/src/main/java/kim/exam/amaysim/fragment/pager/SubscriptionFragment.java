package kim.exam.amaysim.fragment.pager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kim.exam.amaysim.MainActivity;
import kim.exam.amaysim.R;
import kim.exam.amaysim.adapter.SubscriptionAdapter;
import kim.exam.amaysim.model.SubscriptionsInfo;
import kim.exam.amaysim.util.Constant;

/**
 * Created by Kim PC on 12/22/2016.
 */

public class SubscriptionFragment extends Fragment {

    private MainActivity main;
    private ArrayList<SubscriptionsInfo> subscriptionsInfos;

    public static Fragment newInstance(ArrayList<SubscriptionsInfo> subscriptionsInfos) {
        SubscriptionFragment fragment = new SubscriptionFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Constant.BUNDLE_SUBSCRIPTIONS, subscriptionsInfos);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_subscriptions, container, false);

        main = (MainActivity) getActivity();
        subscriptionsInfos = getArguments().getParcelableArrayList(Constant.BUNDLE_SUBSCRIPTIONS);

        setUpViews(view);
        return view;
    }

    private void setUpViews(View v) {
        RecyclerView recycler = (RecyclerView) v.findViewById(R.id.recycler_subscriptions);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(main));
        recycler.setAdapter(new SubscriptionAdapter(main, subscriptionsInfos));
    }
}
