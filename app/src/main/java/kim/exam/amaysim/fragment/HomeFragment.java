package kim.exam.amaysim.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;

import java.io.IOException;
import java.io.InputStream;

import kim.exam.amaysim.MainActivity;
import kim.exam.amaysim.R;
import kim.exam.amaysim.adapter.PagerFragmentAdapter;
import kim.exam.amaysim.model.UserInfo;
import kim.exam.amaysim.util.Constant;

/**
 * Created by Kim PC on 12/22/2016.
 */

public class HomeFragment extends Fragment {

    private MainActivity main;
    private UserInfo userInfo;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_home, container, false);

        main = (MainActivity) getActivity();
        userInfo = getArguments().getParcelable(Constant.BUNDLE_USER_INFO);

        setUpViews(view);
        Log.e("test", "-" + userInfo.firstName + " " + userInfo.lastName);
        return view;
    }

    private void setUpViews(View v) {
        ViewPager viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        viewPager.setAdapter(new PagerFragmentAdapter(getFragmentManager(), userInfo));

        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) v.findViewById(R.id.tabs);
        tabsStrip.setViewPager(viewPager);
    }
}
