package kim.exam.amaysim.fragment.pager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kim.exam.amaysim.MainActivity;
import kim.exam.amaysim.R;
import kim.exam.amaysim.adapter.ServiceAdapter;
import kim.exam.amaysim.model.ServicesInfo;
import kim.exam.amaysim.util.Constant;

/**
 * Created by Kim PC on 12/22/2016.
 */

public class ServiceFragment extends Fragment {

    private MainActivity main;
    private ArrayList<ServicesInfo> servicesInfos;

    public static Fragment newInstance(ArrayList<ServicesInfo> servicesInfos) {
        ServiceFragment fragment = new ServiceFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Constant.BUNDLE_SERVICES, servicesInfos);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_services, container, false);

        main = (MainActivity) getActivity();
        servicesInfos = getArguments().getParcelableArrayList(Constant.BUNDLE_SERVICES);

        setUpViews(view);
        return view;
    }

    private void setUpViews(View v) {
        RecyclerView recycler = (RecyclerView) v.findViewById(R.id.recycler_services);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(main));
        recycler.setAdapter(new ServiceAdapter(main, servicesInfos));
    }
}
