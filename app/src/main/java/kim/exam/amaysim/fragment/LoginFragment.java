package kim.exam.amaysim.fragment;

import android.content.Context;
import android.icu.lang.UScript;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kim.exam.amaysim.MainActivity;
import kim.exam.amaysim.R;
import kim.exam.amaysim.model.ProductInfo;
import kim.exam.amaysim.model.ServicesDataInfo;
import kim.exam.amaysim.model.ServicesInfo;
import kim.exam.amaysim.model.SubscriptionsInfo;
import kim.exam.amaysim.model.UserInfo;
import kim.exam.amaysim.util.Constant;

/**
 * Created by Kim PC on 12/22/2016.
 */

public class LoginFragment extends Fragment implements View.OnClickListener {

    private MainActivity main;
    private EditText editEmail;
    private EditText editPassword;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_login, container, false);

        main = (MainActivity) getActivity();
        setUpViews(view);
        return view;
    }

    private void setUpViews(View v) {
        editEmail = (EditText) v.findViewById(R.id.edit_email);
        editPassword = (EditText) v.findViewById(R.id.edit_password);

        v.findViewById(R.id.button_login).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.button_login:
            String email = editEmail.getText().toString();
            String password = editPassword.getText().toString();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new DoLogin().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, email, password);
            } else {
                new DoLogin().execute(email, password);
            }
            break;
        }
    }

    private class DoLogin extends AsyncTask<String, Long, Boolean> {
        private String msg;
        private UserInfo userInfo = new UserInfo();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                JSONObject jsonCollections = new JSONObject(main.jsonCollections);
                JSONObject jsonData = jsonCollections.getJSONObject("data");
                String email = jsonData.getJSONObject("attributes").getString("email-address");
                String dateOfBirth = jsonData.getJSONObject("attributes").getString("date-of-birth");

                if ( email.equalsIgnoreCase(params[0]) && dateOfBirth.equalsIgnoreCase(params[1]) ) {
                    userInfo.type = jsonData.getString("type");
                    userInfo.id = jsonData.getString("id");

                    JSONObject jsonObject = jsonData.getJSONObject("attributes");
                    userInfo.paymentType = jsonObject.getString("payment-type");
                    userInfo.unbilledCharges = jsonObject.getString("unbilled-charges");
                    userInfo.nextBillingDate = jsonObject.getString("next-billing-date");
                    userInfo.title = jsonObject.getString("title");
                    userInfo.firstName = jsonObject.getString("first-name");
                    userInfo.lastName = jsonObject.getString("last-name");
                    userInfo.dateOfBirth = dateOfBirth;
                    userInfo.contactNumber = jsonObject.getString("contact-number");
                    userInfo.email = email;
                    userInfo.emailVerified = jsonObject.getBoolean("email-address-verified");
                    userInfo.emailSubscriptionStatus = jsonObject.getBoolean("email-subscription-status");

                    jsonObject = jsonData.getJSONObject("links");
                    userInfo.link = jsonObject.getString("self");

                    jsonObject = jsonData.getJSONObject("relationships").getJSONObject("services").getJSONObject("links");
                    userInfo.servicesLink = jsonObject.getString("related");

                    List<ServicesInfo> servicesInfos = new ArrayList<>();
                    List<SubscriptionsInfo> subscriptionsInfos = new ArrayList<>();
                    List<ProductInfo> productInfos = new ArrayList<>();

                    JSONArray jsonArray = jsonCollections.getJSONArray("included");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        String type = jsonObject.getString("type");

                        switch (type) {
                        case "services":
                            ServicesInfo servicesInfo = new ServicesInfo();
                            servicesInfo.type = type;
                            servicesInfo.id = jsonObject.getString("id");

                            jsonData = jsonObject.getJSONObject("attributes");
                            servicesInfo.msn = jsonData.getString("msn");
                            servicesInfo.credit = jsonData.getString("credit");
                            servicesInfo.creditExpiry = jsonData.getString("credit-expiry");
                            servicesInfo.dataUsage = jsonData.getBoolean("data-usage-threshold");

                            jsonData = jsonObject.getJSONObject("links");
                            servicesInfo.link = jsonData.getString("self");

                            jsonData = jsonObject.getJSONObject("relationships").getJSONObject("subscriptions");
                            servicesInfo.subscriptionLinks = jsonData.getJSONObject("links").getString("related");

                            List<ServicesDataInfo> servicesDataInfos = new ArrayList<>();
                            for (int o = 0; o < jsonData.getJSONArray("data").length(); o++) {
                                ServicesDataInfo servicesDataInfo = new ServicesDataInfo();
                                servicesDataInfo.id = jsonData.getJSONArray("data").getJSONObject(i).getString("id");
                                servicesDataInfo.type = jsonData.getJSONArray("data").getJSONObject(i).getString("type");
                                servicesDataInfos.add(servicesDataInfo);
                            }
                            servicesInfos.add(servicesInfo);
                            break;
                        case "subscriptions":
                            SubscriptionsInfo subscriptionsInfo = new SubscriptionsInfo();
                            subscriptionsInfo.type = type;
                            subscriptionsInfo.id = jsonObject.getString("id");

                            jsonData = jsonObject.getJSONObject("attributes");
                            subscriptionsInfo.dataBalance = jsonData.getDouble("included-data-balance");
                            subscriptionsInfo.creditBalance = jsonData.getString("included-credit-balance");
                            subscriptionsInfo.rollOverCreditBalance = jsonData.getString("included-rollover-credit-balance");
                            subscriptionsInfo.rollOverDataBalance = jsonData.getString("included-rollover-data-balance");
                            subscriptionsInfo.internationalTalkBalance = jsonData.getString("included-international-talk-balance");
                            subscriptionsInfo.expiryDate = jsonData.getString("expiry-date");
                            subscriptionsInfo.autoRenewal = jsonData.getBoolean("auto-renewal");
                            subscriptionsInfo.primarySubscription = jsonData.getBoolean("primary-subscription");

                            jsonData = jsonObject.getJSONObject("links");
                            subscriptionsInfo.links = jsonData.getString("self");

                            jsonData = jsonObject.getJSONObject("relationships").getJSONObject("service").getJSONObject("links");
                            subscriptionsInfo.servicesLinks = jsonData.getString("related");

                            jsonData = jsonObject.getJSONObject("relationships").getJSONObject("product").getJSONObject("data");
                            subscriptionsInfo.productType = jsonData.getString("type");
                            subscriptionsInfo.productId = jsonData.getString("id");

                            jsonData = jsonObject.getJSONObject("relationships").getJSONObject("downgrade");
                            subscriptionsInfo.downgrade = jsonData.getString("data");
                            subscriptionsInfos.add(subscriptionsInfo);
                            break;
                        case "products":
                            ProductInfo productInfo = new ProductInfo();
                            productInfo.type = type;
                            productInfo.id = jsonObject.getString("id");

                            jsonData = jsonObject.getJSONObject("attributes");
                            productInfo.name = jsonData.getString("name");
                            productInfo.includedData = jsonData.getString("included-data");
                            productInfo.includedCredit = jsonData.getString("included-credit");
                            productInfo.includedInternationalTalk = jsonData.getString("included-international-talk");
                            productInfo.unliText = jsonData.getBoolean("unlimited-text");
                            productInfo.unliTalk = jsonData.getBoolean("unlimited-talk");
                            productInfo.unliInternationalText = jsonData.getBoolean("unlimited-international-text");
                            productInfo.unliInternationalTalk = jsonData.getBoolean("unlimited-international-talk");
                            productInfo.price = jsonData.getDouble("price");
                            productInfos.add(productInfo);
                            break;
                        }
                    }

                    userInfo.lstServices = servicesInfos;
                    userInfo.lstSubscriptionsInfo = subscriptionsInfos;
                    userInfo.lstProduct = productInfos;
                    return true;
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {

            if ( result ) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constant.BUNDLE_USER_INFO, userInfo);

                HomeFragment homeFragment = new HomeFragment();
                homeFragment.setArguments(bundle);
                main.switchFragment(homeFragment);
                Toast.makeText(main, getString(R.string.msg_success_to_login), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(main, getString(R.string.msg_failed_to_login), Toast.LENGTH_LONG).show();
            }
        }
    }
}
