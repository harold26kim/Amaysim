package kim.exam.amaysim.util;

/**
 * Created by Kim PC on 12/22/2016.
 */

public class Constant {
    public static final String BUNDLE_USER_INFO         =       "user_info";
    public static final String BUNDLE_PRODUCTS          =       "products";
    public static final String BUNDLE_SERVICES          =       "services";
    public static final String BUNDLE_SUBSCRIPTIONS     =       "subscriptions";
}
